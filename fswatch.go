package main

import (
	"bufio"
	log "github.com/bdlm/log"
	"os/exec"
)

func fswatch() {
	for {
		// Recurse and halt operation after first event
		// In the future, we'll scan through results for
		// certain events.
		cmd := exec.Command("fswatch", "-r1", *flag_directory)

		stdout, err := cmd.StdoutPipe()
		if err != nil {
			log.WithField("error", err).Panic("There was a fatal error creating a stdout pipe on fswatch")
			panic("cmd.StdoutPipe() failed")
		}

		if err := cmd.Start(); err != nil {
			log.WithField("error", err).Panic("There was a fatal error starting command fswatch!")
			panic("cmd.Start() failed")
		}

		scanner := bufio.NewScanner(stdout)

		if successful := scanner.Scan(); successful == false {
			err = scanner.Err()
			if err != nil {
				log.WithField("error", err).Error("There was an error reading from stdout")
			}
		}

		if err = cmd.Wait(); err != nil {
			log.WithField("error", err).Error("There was an error waiting for the fswatch command to finish")
		}

		executeCommands()
	}
}
