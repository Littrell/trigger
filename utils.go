package main

import (
	log "github.com/bdlm/log"
	"io"
	"os"
	"os/exec"
	"strings"
)

var commandProcesses []*os.Process

func executeCommands() {
	// Kill processes from previous run
	for _, cmdPcs := range commandProcesses {
		if err := cmdPcs.Kill(); err != nil {
			log.WithField("error", err).Error("There was an error when killing process")
		}
	}

	// Remove references to processes
	commandProcesses = nil

	// Execute commands
	for _, command := range flag_commands {
		executeCommand(command)
	}
}

func executeCommand(command string) {
	cmdSplit := strings.Fields(command)
	cmdName := cmdSplit[0]
	cmdArgs := cmdSplit[1:len(cmdSplit)]
	cmd := exec.Command(cmdName, cmdArgs...)

	stdoutIn, err := cmd.StdoutPipe()
	if err != nil {
		log.WithField("error", err).Error("There was an error capturing stdout")
	}

	stderrIn, err := cmd.StderrPipe()
	if err != nil {
		log.WithField("error", err).Error("There was an error capturing stderr")
	}

	err = cmd.Start()
	commandProcesses = append(commandProcesses, cmd.Process)

	if err != nil {
		log.WithField("error", err).Error("There was an error starting command")
	} else {
		if *flag_verbose == true {
			log.WithField("command", command).Info("Running command...")
		}
	}

	// Capture stdout
	go copyAndCapture(os.Stdout, stdoutIn)

	// Capture stderr
	go copyAndCapture(os.Stderr, stderrIn)
}

func copyAndCapture(w io.Writer, r io.Reader) ([]byte, error) {
	var out []byte
	buf := make([]byte, 1024, 1024)
	for {
		n, err := r.Read(buf[:])
		if n > 0 {
			d := buf[:n]
			out = append(out, d...)
			_, err := w.Write(d)
			if err != nil {
				return out, err
			}
		}
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			return out, err
		}
	}
}
