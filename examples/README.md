# Gotchas

If you are running this in the WSL on Windows, fswatch will not work! This is a known issue with the WSL! If you need trigger in Docker running on Windows, use --type poll.

Make changes to workdir/changes.md to see how trigger works.

# Dockerfile

This tool can be used to live reload applications in a Dockerfile when a native live reload solution does not exist. It is best suited for a golang environment, however it can be built in any operating system without too much fuss. Consider using multi-stage builds in this case. Build trigger in the first stage and then copy the binary from the first stage to subsequent stages.

```Dockerfile
FROM golang

...Dockerfile content goes here...

RUN go get gitlab.com/Littrell/trigger

ENTRYPOINT ["trigger", "-command", "go build", "-command", "./somebinary"]
```
