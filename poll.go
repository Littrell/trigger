package main

import (
	log "github.com/bdlm/log"
	"gopkg.in/pipe.v2"
	"time"
)

func poll() {
	var (
		cmdParams    []string
		excludedDirs []string
		md5sum       string
	)

	// Start setting up the command arguments from flags
	if len(flag_exclude_dirs) > 0 {
		for _, element := range flag_exclude_dirs {
			excludedDirs = append(excludedDirs, "-not")
			excludedDirs = append(excludedDirs, "-path")
			excludedDirs = append(excludedDirs, element)
		}
	}

	if *flag_git == false {
		cmdParams = append(cmdParams, *flag_directory)
		cmdParams = append(cmdParams, "-type")
		cmdParams = append(cmdParams, "f")
		cmdParams = append(cmdParams, excludedDirs...)
		cmdParams = append(cmdParams, "-name")
		cmdParams = append(cmdParams, *flag_pattern)
		cmdParams = append(cmdParams, "-exec")
		cmdParams = append(cmdParams, "md5sum")
		cmdParams = append(cmdParams, "{}")
		cmdParams = append(cmdParams, "+")
	} else {
		cmdParams = append(cmdParams, "ls-tree")
		cmdParams = append(cmdParams, "-r")
		cmdParams = append(cmdParams, "master")
		cmdParams = append(cmdParams, "--name-only")
	}

	var p pipe.Pipe
	for ; ; time.Sleep(time.Duration(*flag_poll) * time.Millisecond) {
		if *flag_git == false {
			p = pipe.Line(
				pipe.Exec("find", cmdParams...),
				pipe.Exec("sort"),
				pipe.Exec("md5sum"),
			)
		} else {
			p = pipe.Line(
				pipe.Exec("git", cmdParams...),
				pipe.Exec("xargs", "md5sum"),
				pipe.Exec("sort"),
				pipe.Exec("md5sum"),
			)
		}
		out, err := pipe.CombinedOutput(p)
		if err != nil {
			log.WithField("error", err).Panic("There was a fatal error!")
			panic("pipe.CombinedOutput failed")
		}

		outString := string(out)

		// This is the first run. Set the current
		// md5sum, update flag, and keep looping.
		if firstRun == true {
			md5sum = outString
			firstRun = false
			continue
		}

		// Change detected!
		if outString != md5sum {
			md5sum = outString

			if *flag_verbose == true {
				log.WithField("md5", md5sum).Info("Files changed")
			}

			executeCommands()
		}
	}
}
