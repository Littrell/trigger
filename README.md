# trigger

A convenience tool that polls the file system for changes and runs a command on file change. In the case fswatch support is lacking, it falls back to standard Linux commands like 'find' and 'md5sum' to detect changes. trigger works great for live reload. It also works as a pseudo if-this-then-that tool.

## Parameters
| Parameter | Default | Optional | Description |
| --------- | ------- | -------- | ----------- |
| -command | "" | No | Command to execute when a change has been detected. Multiple command arguments may be used and are executed in order. |
| -directory | . | Yes | Directory to watch for changes. |
| -exclude-dir | "" | Yes | Directories to exclude from polling. |
| -git | false | Yes | Only poll files tracked by git. This will ignore -directory, -exclude-dir, and -pattern flags. |
| -pattern | "*" | Yes | File pattern to watch. |
| -poll | 5000 | Yes | Time between polls (in milliseconds)
| -type | fswatch | Yes | Type of polling; poll, fswatch |
| -v | false | Yes | Verbose logging |
| -wait | false | Yes | Wait until a change is detected before running command(s). |

The `-directory`, `-exclude-dir`, and `-pattern` parameters use the Linux find command syntax.

## Build

```bash
go build
```

## Examples

### -command
```bash
./trigger -command "echo 'Command 1" -command "echo 'Command 2'"
```

### -v
```bash
./trigger -v -command "go build"
```

### -wait
```bash
./trigger -wait -command "go build"
```

## Polling Examples

The default type is fswatch. The 'poll' type is the fallback in the case fswatch doesn't exist. It also contains some additional options.

### -pattern
```bash
./trigger -pattern "*.go" -command "go build" -command "./somebinary" -type poll
```

### -exclude-dir
```bash
./trigger -exclude-dir "./vendor/*" -exclude-dir "./.git/*" -command "go build" -command "./somebinary" -type poll
```

### -poll
```bash
./trigger -poll 1000 -command "go build" -command "./somebinary" -type poll
```

## TODOs

* Attempt to replace polling
  * Some systems might not have `md5sum` installed by default.
  * Surely file system changes can be detected in go
