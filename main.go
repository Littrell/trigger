package main

import (
	"flag"
	log "github.com/bdlm/log"
	"os/exec"
)

var firstRun bool

var (
	flag_directory    = flag.String("directory", ".", "Directory to watch for changes")
	flag_git          = flag.Bool("git", false, "Only poll files tracked by git. This flag will ignore -directory, -exclude-dir, and -pattern flags. fswatch ignores this flag.")
	flag_pattern      = flag.String("pattern", "*", "File pattern to watch. fswatch ignores this flag.")
	flag_poll         = flag.Int64("poll", 5000, "Time between polls (in milliseconds). fswatch ignores this flag.")
	flag_verbose      = flag.Bool("v", false, "Verbose logging")
	flag_wait         = flag.Bool("wait", false, "Wait until a change is detected before running pre-command and command")
	flag_type         = flag.String("type", "fswatch", "Type of polling; poll, fswatch")
	flag_exclude_dirs arrayFlags
	flag_commands     arrayFlags
)

type arrayFlags []string

// Helper function for parsing flag_exclude_dirs
func (i *arrayFlags) String() string {
	return ""
}

// Helper function for parsing flag_exclude_dirs
func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func init() {
	// Parse flags
	flag.Var(&flag_exclude_dirs, "exclude-dir", "Directories to exclude from polling")
	flag.Var(&flag_commands, "command", "Commands to execute in order")
	flag.Parse()

	// Set the log format
	log.SetFormatter(&log.TextFormatter{
		ForceTTY: true,
	})

	if len(flag_commands) == 0 {
		log.Panic("Must provide at least one command")
		panic("Must provide at least one command")
	}

	firstRun = true
}

func main() {
	var blockCh chan bool = make(chan bool)

	if *flag_wait == false {
		executeCommands()
	}

	if *flag_type == "fswatch" {
		if _, err := exec.LookPath("fswatch"); err != nil {
			log.Warn("fswatch is not installed! Defaulting to polling...")
			go poll()
		} else {
			go fswatch()
		}
	} else {
		go poll()
	}

	// Keep main thread running
	<-blockCh
}
